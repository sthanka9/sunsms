



<section class="col-lg-10 right-section">

    <ul class="breadcrumb border-btm">
        <li class="">
            <a href="<?=BASE_URL?>index.php/admin/index"> Dashboard </a>
        </li>

        <li class="active">
            Subject
        </li>
    </ul>

    <div class="">
        <div class="tabs-wrapper">
            <ul id="tabs">
                <li><a href="#" name="tab1"><?php if(isset($school)){ ?>Edit Subject<?php } else { ?>Add Subject<?php } ?></a></li>

            </ul>

            <div id="content">
                <div id="tab1">
                    <form class="form-horizontal" id="subject_form" method="post" action="<?=BASE_URL?>index.php/admin/createSubject" enctype="multipart/form-data">



                        <div class="panel-body">

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Select Course <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12 m4">
                                    <select class="form-control select" name="course_id" id="course_id">
                                        <option value="0">Select Course</option>
                                        <?php for($s=0;$s<count($course);$s++){ ?>
                                            <option <?php if(isset($subject)){ if($subject[0]['course_id']==$course[$s]['id_course']){ echo "selected='selected'"; } } ?> value="<?=$course[$s]['id_course']?>"><?=$course[$s]['course_name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Name <span class="clr-red">*</span></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="name" id="name" value="<?php if(isset($subject)){ echo $subject[0]['subject_name']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Code <span class="clr-red">*</span></label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="code" id="code" value="<?php if(isset($subject)){ echo $subject[0]['subject_code']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group" <?php if(!isset($subject)){ ?>style="display: none;"<?php } ?>>
                                <label class="col-md-6 col-xs-12 control-label">Status</label>
                                <div class="col-md-6 col-xs-12">
                                    <select class="form-control select" name="status" id="status">
                                        <option <?php if(isset($subject)){ if($subject[0]['status']==1){ echo "selected='selected'"; } } ?> value="1">Active</option>
                                        <option <?php if(isset($subject)){ if($subject[0]['status']==0){ echo "selected='selected'"; } } ?> value="0">Inactive</option>
                                    </select>

                                </div>
                            </div>




                        </div>
                        <div class="text-center">
                            <button class="btn btn-primary">Save</button>
                        </div>
                        <input type="hidden" name="id_subject" id="id_subject" value="<?php if(isset($subject)){ echo encode($subject[0]['id_subject']); } else { echo 0; } ?>">
                    </form>
                </div>

                </div>
            </div>
        </div>
    </div>

</section>



