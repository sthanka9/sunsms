<div id="left" class="">
    <!-- #menu -->
    <ul id="menu" class="bg-blue dker">
        <li class="nav-header">Quick Shortcuts</li>
        <li class="nav-divider"></li>
        <?php if(isset($menu) && $menu=='school')
        { ?>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/welcome/addSchool">
                    <i class="fa fa-user"></i><span class="link-title"> Add School</span>
                </a>
            </li>
        <?php
        }
        else if(isset($menu) && $menu=='settings')
        { ?>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/admin/academicYear">
                    <i class="fa fa-user"></i><span class="link-title"> Academic year</span>
                </a>
            </li>

            <li class="active">
                <a href="<?=BASE_URL?>index.php/admin/board">
                    <i class="fa fa-user"></i><span class="link-title"> Board</span>
                </a>
            </li>

            <li class="active">
                <a href="<?=BASE_URL?>index.php/admin/course">
                    <i class="fa fa-user"></i><span class="link-title"> Course</span>
                </a>
            </li>

            <li class="active">
                <a href="<?=BASE_URL?>index.php/admin/subject">
                    <i class="fa fa-user"></i><span class="link-title"> Subject</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/category">
                    <i class="fa fa-user"></i><span class="link-title"> Category</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/department">
                    <i class="fa fa-user"></i><span class="link-title"> Department</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/admin/weekdays">
                    <i class="fa fa-user"></i><span class="link-title"> Week days</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/admin/classTiming">
                    <i class="fa fa-user"></i><span class="link-title"> Class Timings</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/admin/timeTable">
                    <i class="fa fa-user"></i><span class="link-title"> Time Table</span>
                </a>
            </li>
        <?php }
        else if(isset($menu) && $menu=='staff')
        { ?>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/staff">
                    <i class="fa fa-user"></i><span class="link-title"> Staff</span>
                </a>
            </li>
        <?php
        }
        else if(isset($menu) && $menu=='transport')
        { ?>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/vehicle">
                    <i class="fa fa-user"></i><span class="link-title"> Vehicle</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/driver">
                    <i class="fa fa-user"></i><span class="link-title"> Driver</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/driver/drivervehicle">
                    <i class="fa fa-user"></i><span class="link-title"> Driver Vehicle</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/route">
                    <i class="fa fa-user"></i><span class="link-title"> Route</span>
                </a>
            </li>
            <li class="active">
                <a href="<?=BASE_URL?>index.php/route/studentroute">
                    <i class="fa fa-user"></i><span class="link-title"> Student Route</span>
                </a>
            </li>
            <?php
        }
        ?>

    </ul><!-- /#menu -->
</div>