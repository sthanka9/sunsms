<div class="body-nav body-nav-horizontal">
    <button class="nav-toggle">
        <b class="mobile-menu-txt">Menu</b>
        <div class="icon-menu"> <span class="line line-1"></span> <span class="line line-2"></span> <span class="line line-3"></span> </div>
    </button>
    <div class="main-top-nav">
        <ul  class="nav-menu menu">
            <?php if($this->session->userdata('user_type_id')==1){ ?>
            <li class="menu-item <?php if(isset($menu) && $menu=='school'){ echo 'active'; } ?>">
                <a href="<?=BASE_URL?>" class="menu-link">
                    <i class="icon-dashboard icon-large"></i> Dashboard
                </a>
            </li>
            <li class="menu-item <?php if(isset($menu) && $menu=='transport'){ echo 'active'; } ?>">
                <a href="<?=BASE_URL?>index.php/vehicle" class="menu-link">
                    <i class="icon-truck icon-large"></i> Transport
                </a>
            </li>
            <?php } if($this->session->userdata('user_type_id')==2){ ?>
                <li class="menu-item <?php if(isset($menu) && $menu=='school-setup'){ echo 'active'; } ?>">
                    <a href="<?=BASE_URL?>index.php/admin/" class="menu-link">
                        <i class="icon-dashboard icon-large"></i> Dashboard
                    </a>
                </li>
                <li class="menu-item <?php if(isset($menu) && $menu=='settings'){ echo 'active'; } ?>">
                    <a class="menu-link" href="<?=BASE_URL?>index.php/admin/academicYear">
                        <i class="icon-cogs icon-large"></i> Settings
                    </a>
                </li>
                <li class="menu-item <?php if(isset($menu) && $menu=='staff'){ echo 'active'; } ?>">
                    <a class="menu-link" href="<?=BASE_URL?>index.php/staff"">
                        <i class="fa fa-user icon-large"></i> Staff
                    </a>
                </li>

            <?php } ?>
        </ul>
    </div>
</div>