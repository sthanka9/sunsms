



<section class="col-lg-10 right-section">

    <ul class="breadcrumb border-btm">
        <li>
            <a href="<?=BASE_URL?>index.php/student/index">Home</a>

        </li>

        <li class="active">
            Dashboard
        </li>
    </ul>

    <div class="">
        <div class="tabs-wrapper">
            <ul id="tabs">
                <li><a href="#" name="tab1"><?php if(isset($student)){ ?>Edit Student<?php } else { ?>Add Student<?php } ?></a></li>

            </ul>

            <div id="content">
                <div id="tab1">
                    <form class="form-horizontal" id="student_form" method="post" action="<?=BASE_URL?>index.php/student/createStudent" enctype="multipart/form-data">



                        <div class="panel-body">
                        <h4><u>Student</u></h4>
                            
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Admission No <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="admission_no" id="admission_no" value="<?php if(isset($student)){ echo $student[0]['admission_number']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                            
                                <label class="col-md-3 col-xs-12 control-label">Admission Date <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="admission_date" id="admission_date" value="<?php if(isset($student)){ echo date('d-m-Y',strtotime($student[0]['admission_date'])); } ?>" class="form-control datepicker"/>
                                    </div>
                                </div>
                            </div>
                            <h4><u>Personal Details</u></h4>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">First Name <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="first_name" id="first_name" value="<?php if(isset($student)){ echo $student[0]['first_name']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Middle Name</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="middle_name" id="middle_name" value="<?php if(isset($student)){ echo $student[0]['middle_name']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Last Name <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="last_name" id="last_name" value="<?php if(isset($student)){ echo $student[0]['last_name']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">National Student ID</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="national_student_id" id="national_student_id" value="<?php if(isset($student)){ echo $student[0]['national_student_id']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                            
                                <label class="col-md-3 col-xs-12 control-label">Date of Birth <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="dob" id="dob" value="<?php if(isset($student)){ echo date('d-m-Y',strtotime($student[0]['dob'])); } ?>" class="form-control datepicker"/>

                                    </div>

                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Gender <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="gender_id" id="gender_id">
                                        <option value="0">Select Gender</option>
                                        <option value="M" <?php if(isset($student)){ if($student[0]['gender']=='M'){ echo "selected='selected'"; } } ?>>Male</option>
                                        <option value="F" <?php if(isset($student)){ if($student[0]['gender']=='F'){ echo "selected='selected'"; } } ?>>FeMale</option>
                                    </select>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Blood Group</label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="blood_group" id="blood_group">
                                        <option value="0">Select Blood Group</option>
                                        <option value="AB+" <?php if(isset($student)){ if($student[0]['blood_group']=='AB+'){ echo "selected='selected'"; } } ?>>AB+</option>
                                        <option value="AB-" <?php if(isset($student)){ if($student[0]['blood_group']=='AB-'){ echo "selected='selected'"; } } ?>>AB-</option>
                                        <option value="A+" <?php if(isset($student)){ if($student[0]['blood_group']=='A+'){ echo "selected='selected'"; } } ?>>A+</option>
                                        <option value="A-" <?php if(isset($student)){ if($student[0]['blood_group']=='A-'){ echo "selected='selected'"; } } ?>>A-</option>
                                        <option value="B+" <?php if(isset($student)){ if($student[0]['blood_group']=='B+'){ echo "selected='selected'"; } } ?>>B+</option>
                                        <option value="B-" <?php if(isset($student)){ if($student[0]['blood_group']=='B-'){ echo "selected='selected'"; } } ?>>B-</option>
                                        <option value="O+" <?php if(isset($student)){ if($student[0]['blood_group']=='O+'){ echo "selected='selected'"; } } ?>>O+</option>
                                        <option value="O-" <?php if(isset($student)){ if($student[0]['blood_group']=='O-'){ echo "selected='selected'"; } } ?>>O-</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Birth Place</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="birthplace" id="birthplace" value="<?php if(isset($student)){ echo $student[0]['birth_place']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Nationality <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="nationality" id="nationality">
                                        <option value="0">Select Nationality</option>
                                        <option value="Indian" <?php if(isset($student)){ if($student[0]['nationality']=='Indian'){ echo "selected='selected'"; } } ?>>Indian</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Language</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="language" id="language" value="<?php if(isset($student)){ echo $student[0]['language']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Religion</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="religion" id="religion" value="<?php if(isset($student)){ echo $student[0]['relegion']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Student Category</label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="student_category" id="student_category">
                                        <option value="0">Select Category</option>
                                        <option value="General" <?php if(isset($student)){ if($student[0]['student_category']=='General'){ echo "selected='selected'"; } } ?>>General</option>
                                        <option value="Special" <?php if(isset($student)){ if($student[0]['student_category']=='Special'){ echo "selected='selected'"; } } ?>>Special</option>
                                    </select>

                                </div>
                            </div>
                            <h4><u>Contact Details</u></h4>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Address Line 1 <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <textarea class="form-control" id="address_line_1" name="address_line_1" rows="3"><?php if(isset($student)){ echo $student[0]['address_line_1']; } ?></textarea>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Address Line 2 <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <textarea class="form-control" id="address_line_2" name="address_line_2" rows="3"><?php if(isset($student)){ echo $student[0]['address_line_2']; } ?></textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Country <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="country_id" id="country_id" onchange="getState(this.value);">
                                        <option value="0">Select Country</option>
                                        <?php for($s=0;$s<count($county);$s++){ ?>
                                            <option <?php if(isset($student)){ if($student[0]['country_id']==$county[$s]['id_country']){ echo "selected='selected'"; } } ?> value="<?=$county[$s]['id_country']?>"><?=$county[$s]['country']?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">State <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="state_id" id="state_id" onchange="getCity(this.value)">
                                        <option value="0">Select State</option>
                                        <?php if(isset($student)){ for($s=0;$s<count($state);$s++){ ?>
                                            <option <?php if(isset($student)){ if($student[0]['state_id']==$state[$s]['id_state']){ echo "selected='selected'"; } } ?> value="<?=$state[$s]['id_state']?>"><?=$state[$s]['state']?></option>
                                        <?php } } ?>
                                    </select>

                                </div>
                            </div>
                           <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">City <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="city_id" id="city_id">
                                        <option value="0">Select City</option>
                                        <?php if(isset($student)){ for($s=0;$s<count($city);$s++){ ?>
                                            <option <?php if(isset($student)){ if($student[0]['city_id']==$city[$s]['id_city']){ echo "selected='selected'"; } } ?> value="<?=$city[$s]['id_city']?>"><?=$city[$s]['city']?></option>
                                        <?php } } ?>
                                    </select>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Pin Code <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="pin_code" id="pin_code" value="<?php if(isset($student)){ echo $student[0]['pincode']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Phone Number</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="phone_number" id="phone_number" value="<?php if(isset($student)){ echo $student[0]['phone_number']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Phone 2</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="alternate_phone" id="alternate_phone" value="<?php if(isset($student)){ echo $student[0]['alternate_phone_number']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Email</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">

                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="email" id="email" value="<?php if(isset($student)){ echo $student[0]['email']; } ?>" class="form-control"/>

                                    </div>

                                </div>
                                <label class="col-md-3 col-xs-12 control-label">Upload Photo <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                    <input type="file" class="fileinput btn-primary " name="profile_photo" id="profile_photo" title="Photo"/>
                                    </div>
                                </div>
                            </div>
                            <?php if(isset($student)){ ?>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label"></label>
                                    <div class="col-md-6 col-xs-12">
                                        <img src="<?=BASE_URL?>uploads/<?=$student[0]['photo']?>" width="150px" height="150px">
                                    </div>
                                </div>
                                <input type="hidden" name="prev_profile_photo" value="<?=$student[0]['photo']?>">
                            <?php } ?>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-default">Clear</button>
                            <button class="btn btn-primary">Save</button>
                        </div>
                    <input type="hidden" name="id_student" id="id_student" value="<?php if(isset($student)){ echo encode($student[0]['id_student']); }else{ echo 0; }?>">
                    <input type="hidden" name="id_user" id="id_user" value="<?php if(isset($student)){ echo encode($student[0]['id_user']); }else{ echo 0; }?>">
                    </form></div>

                </div>
            </div>
        </div>
    </div>

</section>



