<?php
/**
 * Created by PhpStorm.
 * User: ASHOK
 * Date: 29/6/16
 * Time: 8:46 PM
 */ ?>
<section class="col-lg-10 right-section">
    <ul class="breadcrumb border-btm">
        <li class="">
            <?php if(isset($teacher_details)){ ?>Edit Employee<?php } else { ?>Add New Employee<?php } ?>
        </li>
    </ul>
    <div class="">
        <div class="tabs-wrapper">
            <ul id="tabs">
                <li><a href="#" name="tab1"> Teacher Details </a></li>
                <li style="display: <?php if(isset($teacher_details)){ echo 'block'; }else{ echo 'none';} ?> "><a href="#" name="tab2"> Teacher Contact Details </a></li>
                <li style="display: <?php if(isset($teacher_details)){ echo 'block'; }else{ echo 'none';} ?> "><a href="#" name="tab3"> Teacher Documents</a></li>

            </ul>
            <div id="content">
                <div id="tab1">
                    <form class="form-horizontal" id="staff_details" method="post" action="<?=BASE_URL?>index.php/staff/createStaffDetails" enctype="multipart/form-data">
                        <div class="panel-body">
                          <div>
                            <h5>General Details</h5>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Teacher Number <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="teacher_number" id="teacher_number" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['teacher_number']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Joining Date </label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        <input type="text" class="form-control datepicker" name="joining_date" id="joining_date" value="<?php if(isset($teacher_details)){ echo date('d-m-Y',strtotime($teacher_details[0]['joining_date'])); } ?>" onkeypress="return false" title="joining date"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> First Name <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="first_name" id="first_name" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['first_name']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Last Name <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="last_name" id="last_name" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['last_name']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Gender<span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="gender" id="gender">
                                        <option value="0">Select Gender </option>
                                        <option value="1" <?php if(isset($teacher_details)){ if($teacher_details[0]['gender']==1){ echo "selected='selected'"; } } ?>>Male </option>
                                        <option value="2" <?php if(isset($teacher_details)){ if($teacher_details[0]['gender']==2){ echo "selected='selected'"; } } ?>>Female </option>
                                    </select>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Date Of Birth <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        <input type="text" class="form-control datepicker" name="date_of_birth" id="date_of_birth" value="<?php if(isset($teacher_details)){ echo date('d-m-Y',strtotime($teacher_details[0]['dob'])); } ?>" onkeypress="return false"  title="Date Of Birth"/>

                                    </div>
                                </div>
                            </div>
                           <div class="form-group">
                               <label class="col-md-3 col-xs-12 control-label"> Teacher Department </label>
                               <div class="col-md-3 col-xs-12">
                                   <select class="form-control select" name="teacher_department" id="teacher_department">
                                       <option value="0">Select Department </option>
                                       <?php for($s=0;$s<count($department);$s++){ ?>
                                           <option <?php if(isset($teacher_details)){ if($teacher_details[0]['teacher_department']==$department[$s]['id_department']){ echo "selected='selected'"; } } ?> value="<?=$department[$s]['id_department']?>"><?=$department[$s]['department_name']?></option>
                                       <?php } ?>
                                   </select>
                               </div>
                               <label class="col-md-3 col-xs-12 control-label"> Teacher Category </label>
                               <div class="col-md-3 col-xs-12">
                                   <select class="form-control select" name="teacher_category" id="teacher_category">
                                       <option value="0">Select Category </option>
                                       <?php for($s=0;$s<count($category);$s++){ ?>
                                           <option <?php if(isset($teacher_details)){ if($teacher_details[0]['teacher_category']==$category[$s]['id_category']){ echo "selected='selected'"; } } ?> value="<?=$category[$s]['id_category']?>"><?=$category[$s]['category_name']?></option>
                                       <?php } ?>
                                   </select>
                               </div>
                           </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Qualification <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="qualification" id="qualification" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['qualification']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Job Title</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="job_title" id="job_title" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['job_title']; } ?>" class="form-control"/>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <p style="margin-left:75px"> Experience </p>
<!--                                <label class="col-md-3 col-xs-12 control-label">  <span class="clr-red">*</span></label>-->
<!--                                <div class="col-md-3 col-xs-12">-->
<!--                                    <div class="input-group">-->
<!--                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>-->
<!--                                        <input type="text" name="experience" id="experience" value="--><?php //if(isset($teacher_details)){ echo $teacher_details[0]['experience']; } ?><!--" class="form-control"/>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <label class="col-md-3 col-xs-12 control-label"> Years <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="year_id" id="year_id">
                                        <option value=""> Year </option>
                                        <option value="0" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==0){ echo "selected='selected'"; } } ?>>0</option>
                                        <option value="1" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==1){ echo "selected='selected'"; } } ?>>1</option>
                                        <option value="2" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==2){ echo "selected='selected'"; } } ?>>2</option>
                                        <option value="3" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==3){ echo "selected='selected'"; } } ?>>3</option>
                                        <option value="4" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==4){ echo "selected='selected'"; } } ?>>4</option>
                                        <option value="5" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==5){ echo "selected='selected'"; } } ?>>5</option>
                                        <option value="6" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==6){ echo "selected='selected'"; } } ?>>6</option>
                                        <option value="7" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==7){ echo "selected='selected'"; } } ?>>7</option>
                                        <option value="8" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==8){ echo "selected='selected'"; } } ?>>8</option>
                                        <option value="9" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==9){ echo "selected='selected'"; } } ?>>9</option>
                                        <option value="10" <?php if(isset($teacher_details)){ if($teacher_details[0]['year_id']==10){ echo "selected='selected'"; } } ?>>10</option>
                                    </select>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Months <span class="clr-red">*</span></label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="months_id" id="months">
                                        <option value=""> Month </option>
                                        <option value="0" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==0){ echo "selected='selected'"; } } ?>>0</option>
                                        <option value="1" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==1){ echo "selected='selected'"; } } ?>>1</option>
                                        <option value="2" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==2){ echo "selected='selected'"; } } ?>>2</option>
                                        <option value="3" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==3){ echo "selected='selected'"; } } ?>>3</option>
                                        <option value="4" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==4){ echo "selected='selected'"; } } ?>>4</option>
                                        <option value="5" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==5){ echo "selected='selected'"; } } ?>>5</option>
                                        <option value="6" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==6){ echo "selected='selected'"; } } ?>>6</option>
                                        <option value="7" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==7){ echo "selected='selected'"; } } ?>>7</option>
                                        <option value="8" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==8){ echo "selected='selected'"; } } ?>>8</option>
                                        <option value="9" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==9){ echo "selected='selected'"; } } ?>>9</option>
                                        <option value="10" <?php if(isset($teacher_details)){ if($teacher_details[0]['months_id']==10){ echo "selected='selected'"; } } ?>>10</option>
                                    </select>
                                </div>
                            </div>
                              <div class="form-group">
                                  <label class="col-md-3 col-xs-12 control-label"> Phone Number <span class="clr-red">*</span></label>
                                  <div class="col-md-3 col-xs-12">
                                      <div class="input-group">
                                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                          <input type="text" name="phone_number" id="phone_number" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['phone_number']; } ?>" class="form-control"/>
                                      </div>
                                  </div>
                                  <label class="col-md-3 col-xs-12 control-label"> Email <span class="clr-red">*</span></label>
                                  <div class="col-md-3 col-xs-12">
                                      <div class="input-group">
                                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                          <input type="text" name="email" id="email" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['email']; } ?>" class="form-control"/>
                                      </div>
                                  </div>

                              </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Experience Details</label>
                                <div class="col-md-9 col-xs-12">
                                    <textarea rows="5" class="form-control" name="experience_details" id="experience_details"><?php if(isset($teacher_details)){ echo $teacher_details[0]['experience_details']; } ?></textarea>
                                </div>
                            </div>
                          <div class="form-group">
                              <label class="col-md-3 col-xs-12 control-label"> Status<span class="clr-red">*</span></label>
                              <div class="col-md-3 col-xs-12">
                                  <select class="form-control select" name="staff_status" id="staff_status">
                                      <option value="1" <?php if(isset($teacher_details)){ if($teacher_details[0]['staff_status']==1){ echo "selected='selected'"; } } ?>>Active </option>
                                      <option value="0" <?php if(isset($teacher_details)){ if($teacher_details[0]['staff_status']==0){ echo "selected='selected'"; } } ?>>InActive </option>
                                  </select>
                              </div>
                          </div>
                       </div>
                        <div>
                            <h5>Personal Details</h5>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Marital Status </label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="marital_status" id="marital_status">
                                        <option value="1" <?php if(isset($teacher_details)){ if($teacher_details[0]['marital_status']==1){ echo "selected='selected'"; } } ?>>Married </option>
                                        <option value="2" <?php if(isset($teacher_details)){ if($teacher_details[0]['marital_status']==2){ echo "selected='selected'"; } } ?>>UnMarried </option>
                                    </select>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Blood Group </label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="blood_group" id="blood_group">
                                        <option value="0">Select Blood Group </option>
                                        <?php for($s=0;$s<count($blood_group);$s++){ ?>
                                            <option <?php if(isset($teacher_details)){ if($teacher_details[0]['blood_group_id']==$blood_group[$s]['id_blood_group']){ echo "selected='selected'"; } } ?> value="<?=$blood_group[$s]['id_blood_group']?>"><?=$blood_group[$s]['name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Father Name</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="father_name" id="father_name" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['father_name']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Mother Name</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="mother_name" id="mother_name" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['mother_name']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Children Count</label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="children_count" id="children_count" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['children_count']; } ?>" class="form-control"/>
                                    </div>
                                </div>
                                <label class="col-md-3 col-xs-12 control-label"> Nationality </label>
                                <div class="col-md-3 col-xs-12">
                                    <select class="form-control select" name="nationality_id" id="nationality_id">
                                        <option value="">Select Blood Group </option>
                                        <option value="1" <?php if(isset($teacher_details)){ if($teacher_details[0]['nationality_id']==1){ echo "selected='selected'"; } } ?>>Indian </option>
                                        <option value="2" <?php if(isset($teacher_details)){ if($teacher_details[0]['nationality_id']==2){ echo "selected='selected'"; } } ?>>Us </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"> Upload Photo </label>
                                <div class="col-md-3 col-xs-12">
                                    <div class="input-group">
                                        <input type="file" class="fileinput btn-primary" name="profile_pic"  title="logo"/>
                                    </div>
                                </div>
                            </div>

<!--                          <div class="form-group">-->
<!--                                <label class="col-md-3 col-xs-12 control-label"> Address</label>-->
<!--                                <div class="col-md-9 col-xs-12">-->
<!--                                    <textarea rows="5" class="form-control" name="address" id="address">--><?php //if(isset($teacher_details)){ echo $teacher_details[0]['address']; } ?><!--</textarea>-->
<!--                                </div>-->
<!--                            </div>-->

                        </div>
                      </div>
                        <div class="text-center">
                            <button class="btn btn-primary"><?php if(isset($teacher_details)){ ?>Update<?php } else { ?>Save<?php } ?></button>
                        </div>
                        <input type="hidden" name="id_staff" id="id_staff" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['id_staff']; } else { echo 0; } ?>">
                    </form>
                </div>
                <div id="tab2">
                    <form class="form-horizontal" id="staff_contact_details" method="post" action="<?=BASE_URL?>index.php/staff/createStaffContactDetails" >
                        <div class="panel-body">
                          <div>
                              <h5>Home Address</h5>
                              <div class="form-group">
                                  <label class="col-md-3 col-xs-12 control-label"> Home Address Line 1 <span class="clr-red">*</span></label>
                                  <div class="col-md-3 col-xs-12">
                                      <div class="input-group">
                                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                          <input type="text" name="home_address_line1" id="home_address_line1" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['home_address_line1']; } ?>" class="form-control"/>
                                      </div>
                                  </div>
                                  <label class="col-md-3 col-xs-12 control-label"> Home Address Line 2 </label>
                                  <div class="col-md-3 col-xs-12">
                                      <div class="input-group">
                                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                          <input type="text" name="home_address_line2" id="home_address_line2" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['home_address_line2']; } ?>" class="form-control"/>
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-3 col-xs-12 control-label"> Home City <span class="clr-red">*</span></label>
                                  <div class="col-md-3 col-xs-12">
                                      <div class="input-group">
                                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                          <input type="text" name="home_city" id="home_city" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['home_city']; } ?>" class="form-control"/>
                                      </div>
                                  </div>
                                  <label class="col-md-3 col-xs-12 control-label"> Home State <span class="clr-red">*</span></label>
                                  <div class="col-md-3 col-xs-12">
                                      <div class="input-group">
                                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                          <input type="text" name="home_state" id="home_state" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['home_state']; } ?>" class="form-control"/>
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-3 col-xs-12 control-label"> Home Country <span class="clr-red">*</span></label>
                                  <div class="col-md-3 col-xs-12">
                                      <select class="form-control select" name="home_country_id" id="home_country_id" onchange="getState(this.value);">
                                          <option value="0">Select Country</option>
                                          <?php for($s=0;$s<count($county);$s++){ ?>
                                              <option <?php if(isset($teacher_details)){ if($teacher_details[0]['home_country_id']==$county[$s]['id_country']){ echo "selected='selected'"; } } ?> value="<?=$county[$s]['id_country']?>"><?=$county[$s]['country']?></option>
                                          <?php } ?>
                                      </select>

                                  </div>
                                  <label class="col-md-3 col-xs-12 control-label"> Home Pincode <span class="clr-red">*</span></label>
                                  <div class="col-md-3 col-xs-12">
                                      <div class="input-group">
                                          <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                          <input type="text" name="home_pincode" id="home_pincode" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['home_pincode']; } ?>" class="form-control"/>
                                      </div>
                                  </div>
                              </div>

                          </div>
                            <input type="hidden" name="user_id" id="user_id" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['user_id']; } ?>" class="form-control"/>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-primary"><?php if(isset($teacher_details)){ ?>Update<?php } else { ?>Save<?php } ?></button>
                        </div>
                        <input type="hidden" name="deleted_documents_id" id="id_document" value=""/>
                        <input type="hidden" name="id_staff" id="id_staff" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['id_staff']; } else { echo 0; } ?>">
                    </form>
                </div>
                <div id="tab3">
                    <form class="form-horizontal" id="staff_documents" method="post" action="<?=BASE_URL?>index.php/staff/createStaffDocuments" enctype="multipart/form-data">
                        <div class="panel-body">
                            <div id="existingDocumentDiv">
                                <?php if(isset($teacher_documents)){
                                    if(count($teacher_documents)>0){?>
                                        <h4>
                                            <u>Documents</u>
                                        </h4>
                                        <ol>
                                        <?php for($s=0;$s<count($teacher_documents);$s++){ ?>
                                            <li class="li-group">
                                                <span class=""><a href="<?php echo BASE_URL.'uploads/'.$teacher_documents[$s]['document_source'] ;?>" target="_blank"> <?PHP echo $teacher_documents[$s]['document_name'];?></a> <span><a id="<?php echo $teacher_documents[$s]['id_staff_document']?>" title="delete" class="edit" onclick="deleteDocuments(this)"><span class="circle"><i class="fa fa-trash"></i></span></a></a></span></span>
                                            </li>
                                        <?php } ?>
                                        </ol>
                                    <?php } ?>
                                <?php }?>
                            </div>
                            <div id="doumentsDiv">
                                <div class="form-group li-group">
                                    <label class="col-md-3 col-xs-12 control-label"> Documents <span class="clr-red">*</span></label>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="input-group">
                                            <input type="file" class="fileinput btn-primary document " name="documents[]"  title="logo"/>
                                            <label class="error" style="display: none">Please Select Document</label>
                                        </div>
                                        <span class="deleteSpan" ><a title="delete" onclick="deleteNewlyAddDocument(this)"><span class="circle"><i class="fa fa-trash"></i></span></a></a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-primary" onclick="addAnotherDocument();">Add Another</a>
                            <a class="btn btn-primary" onclick="checkDocumentValidation();">Save</a>
                        </div>
                        <input type="hidden" name="deleted_documents_id" id="id_document" value=""/>
                        <input type="hidden" name="id_staff" id="id_staff" value="<?php if(isset($teacher_details)){ echo $teacher_details[0]['id_staff']; } else { echo 0; } ?>">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(function(){
        $(".tabs-wrapper #content").find("[id^='tab']").hide(); // Hide all content
        $(".tabs-wrapper #tabs li:first").attr("id","current"); // Activate the first tab
        $(".tabs-wrapper #content #tab1").fadeIn(); // Show first tab's content

        $('#tabs a').click(function(e) {
            e.preventDefault();
            if ($(this).closest("li").attr("id") == "current"){ //detection for current tab
                return;
            }
            else{
                $(".tabs-wrapper #content").find("[id^='tab']").hide(); // Hide all content
                $(".tabs-wrapper #tabs li").attr("id",""); //Reset id's
                $(this).parent().attr("id","current"); // Activate this
                $('#' + $(this).attr('name')).fadeIn(); // Show content for the current tab
            }
        });
    });
    function addAnotherDocument(){
        var _html= '<div class="form-group li-group">'+
            '<label class="col-md-3 col-xs-12 control-label"> Documents <span class="clr-red">*</span></label>'+
            '<div class="col-md-3 col-xs-12">'+
            '<div class="input-group">'+
            '<input type="file" class="fileinput btn-primary document " name="documents[]"  title="logo"/>'+
            '<label class="error" style="display: none">Please Select Document</label>' +
            '</div>'+
            '<span class="deleteSpan"><a title="delete" onclick="deleteNewlyAddDocument(this)"><span class="circle"><i class="fa fa-trash"></i></span></a></a></span>'+
            '</div>'+
            '</div>';
        $('#doumentsDiv').append(_html);
    }

    function deleteNewlyAddDocument(e){
        $(e).parent().closest('.li-group').remove();
        if($('#doumentsDiv').find('.form-group').length==0){
            addAnotherDocument();
        }
    }

    function deleteDocuments(e){
        var _id=$(e).attr('id');
        $(e).parent().closest('.li-group').remove();
        var all_ids=$('#id_document').val();
        if(all_ids==''){
            $('#id_document').val(_id);
        }else{
            all_ids=all_ids+','+_id;
            $('#id_document').val(all_ids);
        }
    }

    function checkDocumentValidation()
    {
        var flag=0;
        var doc_length=$('#existingDocumentDiv').find('.li-group').length;
        $('.document').each(function(){
            if($(this).val()!=''){
                var _parent= $(this).parent();
                $(_parent).find('.error').hide();
                var flDoc=$(this).val();
                var flExt=flDoc.substring(flDoc.lastIndexOf('.')+1);
                //alert(flExt);
                if(flExt == "doc" || flExt == "docx" || flExt == "pdf" || flExt == "txt")
                {
                }else{
                    flag=1;
                    $(_parent).find('.error').text('Invalid Format It Allows doc, docx , pdf , txt').show();
                }
            }else if( doc_length==0){
                if($('#id_staff').val()!=0){
                    var _parent= $(this).parent();
                    $(_parent).find('.error').text('Please Select Document').show();
                    flag=1;
                }
            }
        });
        if(flag==1){
            return false;
        }else{
            $('#staff_documents').submit();
        }
    }
</script>