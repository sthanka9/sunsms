<?php
/**
 * Created by PhpStorm.
 * User: ASHOK
 * Date: 28/6/16
 * Time: 10:54 PM
 */ ?>
<section class="col-lg-10 right-section">

    <ul class="breadcrumb border-btm">
        <li class="">
            <a href="<?=BASE_URL?>index.php/admin/index"> Dashboard </a>
        </li>

        <li class="active">
            Staff
        </li>
    </ul>

    <a href="<?=BASE_URL?>index.php/staff/createStaffView" class="btn btn-primary m8 float-right p6">Create New Employee</a>

    <table id="table" class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Teacher Number</th>
            <th>Employee Name</th>
            <th>Gender</th>
            <th>Qualification</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>



        </tbody>

    </table>

</section>





<script type="text/javascript">
    $(function () {

        getStaffDataTable();


    });
</script>