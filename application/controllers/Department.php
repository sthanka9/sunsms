<?php
/**
 * Created by PhpStorm.
 * User: ASHOK
 * Date: 28/6/16
 * Time: 10:02 PM
 */

class Department extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Datatables');
        $this->load->model("mdepartment");

        if($this->session->userdata('user_id') && $this->session->userdata('user_type_id')==2){}
        else{
            redirect(BASE_URL);
        }
    }

    function index()
    {
        $data['header']="header";
        $data['left_menu']="left_menu";
        $data['middle_content']='admin/department';
        $data['footer']='footer';
        $data['menu'] = 'settings';
        $this->load->view('landing',$data);
    }

    function getDepartmentDataTable(){
        $results = json_decode($this->mdepartment->getDepartmentDataTable($_POST));

        for($s=0;$s<count($results->data);$s++)
        {
            $results->data[$s][1] = encode($results->data[$s][1]);
        }
        echo json_encode($results);
    }


    function addUpdateDepartmentView($category_id=0)
    {
        if($category_id===0){}
        else{
            $data['department_details'] = $this->mdepartment->getDepartment(array('department_id' => decode($category_id)));
        }
        $data['header']="header";
        $data['left_menu']="left_menu";
        $data['middle_content']='admin/add_update_department';
        $data['footer']='footer';
        $data['menu'] = 'settings';
        $this->load->view('landing',$data);
    }

    function insertUpdateDepartment()
    {
        if(isset($_POST))
        {
            if(!$_POST['id_department'])
            {
                $this->mdepartment->insertDepartment(array(
                    'department_name' => $_POST['department_name']
                ));
            }
            else
            {
                $this->mdepartment->updateDepartment(array(
                    'id_department' => decode($_POST['id_department']),
                    'department_name' => $_POST['department_name']
                ));
            }

            redirect(BASE_URL.'index.php/department');
        }
    }


    function deleteDepartment($id)
    {
        $this->mdepartment->deleteDepartment(decode($id));
        echo json_encode(array('response' => 1,'data' =>''));
    }
} 