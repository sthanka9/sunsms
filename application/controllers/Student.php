<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Datatables');
		$this->load->model("mstudent");
                $this->load->model("mwelcome");
		if($this->session->userdata('user_id') && $this->session->userdata('user_type_id')==2){
			redirect(BASE_URL.'index.php/admin');
		}
	}

	function index()
	{
		if($this->session->userdata('user_id'))
		{
			$data['header']="header";
			$data['left_menu']="left_menu";
			$data['middle_content']='student';
			$data['footer']='footer';
			$data['menu'] = 'student';
			$this->load->view('landing',$data);
		}
		else
		{
			$data['header']="header";
			$data['left_menu']="";
			$data['middle_content']='login';
			$data['footer']='footer';
			$this->load->view('landing',$data);
		}
	}

	

	

	function getStudentDataTable()
	{
		$results = json_decode($this->mstudent->getStudentDataTable($_POST));
                    
		for($s=0;$s<count($results->data);$s++)
		{
                    $results->data[$s][3] = encode($results->data[$s][3]);
		}
		echo json_encode($results);
	}

	function addStudent($student_id=0)
	{
		if($student_id===0){	}
		else{
			$data['student'] = $this->mstudent->getStudent(array('id_student' => decode($student_id)));
			if(empty($data['student'])){ unset($data['student']); }
			else{
				$data['state'] = $this->mwelcome->getState(array('country_id' => $data['student'][0]['country_id']));
				$data['city'] = $this->mwelcome->getCity(array('state_id' => $data['student'][0]['state_id']));
			}
		}
		$data['county'] = $this->mwelcome->getCountry();
		$data['header']="header";
		$data['left_menu']="";
		$data['middle_content']='add_student';
		$data['footer']='footer';
                $this->load->view('landing',$data);
	}

	function createStudent()
	{
		if($this->session->userdata('user_id'))
		{
			if(isset($_FILES['profile_photo']['name']) && $_FILES['profile_photo']['name']!=''){
				$file_type = check_file_type($_FILES['profile_photo']['name'],'image');
				if($file_type) {
					$school_image = do_upload($_FILES['profile_photo']['name'], $_FILES['profile_photo']['tmp_name'], $this->session->userdata('school_id'));
					$_POST['profile_photo'] = $school_image;
				}
			}
			else{
				$_POST['profile_photo'] = '';
			}

			if(!$_POST['id_student'])
			{
				$password = generate_password(6);
				$user_id = $this->mwelcome->addUser(array(
					'user_type_id' => 5,
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
					'email' => $_POST['email'],
					'password' => md5($password),
					'phone_number' => $_POST['phone_number']
                                ));

				$this->mstudent->addStudent(array(
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
                                        'middle_name' => $_POST['middle_name'],
                                        'gender' => $_POST['gender_id'],
                                        'dob' => date('Y-m-d',strtotime($_POST['dob'])),
                                        'admission_number' => $_POST['admission_no'],
                                        'admission_date' => date('Y-m-d',strtotime($_POST['admission_date'])),
                                        'national_student_id' => $_POST['national_student_id'],
                                        'blood_group' => $_POST['blood_group'],
                                        'birth_place' => $_POST['birthplace'],
                                        'nationality' => $_POST['nationality'],
                                        'language' => $_POST['language'],
                                        'relegion' => $_POST['religion'],
                                        'student_category' => $_POST['student_category'],
                                        'address_line_1' => $_POST['address_line_1'],
                                        'address_line_2' => $_POST['address_line_2'],
                                        'fk_city_id' => $_POST['city_id'],
                                        'pincode' => $_POST['pin_code'],
                                        'phone_number' => $_POST['phone_number'],
                                        'alternate_phone_number' => $_POST['alternate_phone'],
					'fk_id_user' => $user_id,
                                        'photo' => $_POST['profile_photo']
				));
			}
			else
			{
				if($_POST['profile_photo']=='')
					$_POST['profile_photo'] = $_POST['prev_profile_photo'];
                                        $user_id = $this->mwelcome->updateUser(array(
					'id_user' => decode($_POST['id_user']),
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
					'phone_number' => $_POST['phone_number']
				));

				$this->mstudent->updateStudent(array(
					'id_student' => decode($_POST['id_student']),
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
                                        'middle_name' => $_POST['middle_name'],
                                        'gender' => $_POST['gender_id'],
                                        'dob' => date('Y-m-d',strtotime($_POST['dob'])),
                                        'admission_number' => $_POST['admission_no'],
                                        'admission_date' => date('Y-m-d',strtotime($_POST['admission_date'])),
                                        'national_student_id' => $_POST['national_student_id'],
                                        'blood_group' => $_POST['blood_group'],
                                        'birth_place' => $_POST['birthplace'],
                                        'nationality' => $_POST['nationality'],
                                        'language' => $_POST['language'],
                                        'relegion' => $_POST['religion'],
                                        'student_category' => $_POST['student_category'],
                                        'address_line_1' => $_POST['address_line_1'],
                                        'address_line_2' => $_POST['address_line_2'],
                                        'fk_city_id' => $_POST['city_id'],
                                        'pincode' => $_POST['pin_code'],
                                        'phone_number' => $_POST['phone_number'],
                                        'alternate_phone_number' => $_POST['alternate_phone'],
					'fk_id_user' => decode($_POST['id_user']),
                                        'photo' => $_POST['profile_photo']
				));
			}

			redirect(BASE_URL.'index.php/student/index');
		}
		else
		{
			redirect(BASE_URL);
		}
	}

	

	

}
