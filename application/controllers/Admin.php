<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Datatables');
		$this->load->model("mwelcome");

		if($this->session->userdata('user_id') && $this->session->userdata('user_type_id')==2){}
		else{
			redirect(BASE_URL);
		}
	}

	function index()
	{
		$data['school'] = $this->mwelcome->getSchool(array('id_school' => $this->session->userdata('school_id')));
		$data['state'] = $this->mwelcome->getState(array('country_id' => $data['school'][0]['country_id']));
		$data['city'] = $this->mwelcome->getCity(array('state_id' => $data['school'][0]['state_id']));
		$data['county'] = $this->mwelcome->getCountry();

		$data['header']="header";
		$data['left_menu']="left_menu";
		//$data['middle_content']='admin/dashboard';
		$data['middle_content']='admin/school_setup';
		$data['footer']='footer';
		$data['menu'] = 'school-setup';
		$this->load->view('landing',$data);
	}

	function logout()
	{
		$this->session->set_userdata( array(
				'user_id' => '',
				'user_role_id' => '',
				'user_name' => '',
				'user_email' => '',
				'school_name' => '',
				'school_id' => ''
			)
		);

		$this->session->unset_userdata();
		redirect(BASE_URL.'index.php/welcome/index');
	}

	function updateSchool()
	{
		if(isset($_FILES['school_logo']['name']) && $_FILES['school_logo']['name']!=''){
			$file_type = check_file_type($_FILES['school_logo']['name'],'image');
			if($file_type) {
				$school_image = do_upload($_FILES['school_logo']['name'], $_FILES['school_logo']['tmp_name'], $this->session->userdata('school_id'));
				$_POST['school_logo'] = $school_image;
			}
		}
		else{
			$_POST['school_logo'] = $_POST['prev_school_logo'];
		}
		if(isset($_FILES['fav_icon']['name']) && $_FILES['fav_icon']['name']!=''){
			$file_type = check_file_type($_FILES['fav_icon']['name'],'image');
			if($file_type) {
				$school_image = do_upload($_FILES['fav_icon']['name'], $_FILES['fav_icon']['tmp_name'], $this->session->userdata('school_id'));
				$_POST['fav_icon'] = $school_image;
			}
		}
		else{
			$_POST['fav_icon'] = $_POST['prev_fav_icon'];
		}

		$this->mwelcome->updateSchool(array(
			'id_school' => $this->session->userdata('school_id'),
			'school_name' => $_POST['school_name'],
			'registration_id' => $_POST['registration_id'],
			'founded_on' => date('Y-m-d',strtotime($_POST['founded_on'])),
			'curriculam' => $_POST['curriculam'],
			'school_logo' => $_POST['school_logo'],
			'country_id' => $_POST['country_id'],
			'state_id' => $_POST['state_id'],
			'city_id' => $_POST['city_id'],
			'address' => $_POST['address'],
			'pincode' => $_POST['pincode'],
			'phone' => $_POST['phone'],
			'alternative_phone' => $_POST['alternative_phone'],
			'email' => $_POST['email'],
			'fax' => $_POST['fax'],
			'principle_name' => $_POST['principle_name'],
			'principle_email' => $_POST['principle_email'],
			'principle_phone' => $_POST['principle_phone'],
			'principle_mobile' => $_POST['principle_mobile'],
		));


		redirect(BASE_URL.'index.php/admin/');
	}

	function academicYear()
	{
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/academic_year';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function getAcademicYearDataTable()
	{
		$results = json_decode($this->mwelcome->getAcademicYearDataTable($_POST));

		for($s=0;$s<count($results->data);$s++)
		{
			$results->data[$s][4] = encode($results->data[$s][4]);
		}
		echo json_encode($results);
	}

	function addAcademicYear($academic_year=0)
	{
		if($academic_year===0){}
		else{
			$data['academic_year'] = $this->mwelcome->getAcademicYear(array('id_academic_year' => decode($academic_year)));
		}
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/add_academic_year';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function createAcademicYear()
	{
		if(isset($_POST))
		{
			if(!isset($_POST['status'])){ $status=0; }
			else{ $status = $_POST['status']; }

			if(!$_POST['id_academic_year'])
			{
				$this->mwelcome->addAcademicYear(array(
					'school_id' => $this->session->userdata('school_id'),
					'name' => $_POST['name'],
					'start_date' => date('Y-m-d',strtotime($_POST['start_date'])),
					'end_date' => date('Y-m-d',strtotime($_POST['end_date'])),
					'description' => $_POST['description'],
					'status' => $status
				));
			}
			else
			{
				$this->mwelcome->updateAcademicYear(array(
					'id_academic_year' => decode($_POST['id_academic_year']),
					'name' => $_POST['name'],
					'start_date' => date('Y-m-d',strtotime($_POST['start_date'])),
					'end_date' => date('Y-m-d',strtotime($_POST['end_date'])),
					'description' => $_POST['description'],
					'status' => $status
				));
			}

			redirect(BASE_URL.'index.php/admin/academicYear');
		}
	}

	function deleteAcademicYear($id)
	{
		$this->mwelcome->deleteAcademicYear(decode($id));
		echo json_encode(array('response' => 1,'data' =>''));
	}

	function board()
	{
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/board';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function getBoardDataTable()
	{
		$results = json_decode($this->mwelcome->getBoardDataTable($_POST));

		for($s=0;$s<count($results->data);$s++)
		{
			$results->data[$s][2] = encode($results->data[$s][2]);
		}
		echo json_encode($results);
	}

	function addBoard($board_id=0)
	{
		if($board_id===0){}
		else{
			$data['board'] = $this->mwelcome->getBoard(array('id_board' => decode($board_id)));
		}

		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/add_board';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function createBoard()
	{
		//echo "<pre>";print_r($_POST); exit;
		if(isset($_POST))
		{
			if(!isset($_POST['status'])){ $status=0; }
			else{ $status = $_POST['status']; }

			if(!$_POST['id_board'])
			{
				$this->mwelcome->addBoard(array(
					'school_id' => $this->session->userdata('school_id'),
					'board_name' => $_POST['name'],
					'board_description' => $_POST['description'],
					'status' => $status
				));
			}
			else
			{
				$this->mwelcome->updateBoard(array(
					'id_board' => decode($_POST['id_board']),
					'board_name' => $_POST['name'],
					'board_description' => $_POST['description'],
					'status' => $status
				));
			}

			redirect(BASE_URL.'index.php/admin/board');
		}
	}

	function deleteBoard($id)
	{
		$this->mwelcome->deleteBoard(decode($id));
		echo json_encode(array('response' => 1,'data' =>''));
	}

	function course()
	{
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/class';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function getCourseDataTable()
	{
		$results = json_decode($this->mwelcome->getCourseDataTable($_POST));

		for($s=0;$s<count($results->data);$s++)
		{
			$results->data[$s][4] = encode($results->data[$s][4]);
		}
		echo json_encode($results);
	}

	function addCourse($board_id=0)
	{
		if($board_id===0){}
		else{
			$data['course'] = $this->mwelcome->getCourse(array('id_course' => decode($board_id)));
		}
		$data['board'] = $this->mwelcome->getBoard(array('school_id' => $this->session->userdata('school_id'),'status' => 1));
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/add_class';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function createCourse()
	{
		//echo "<pre>";print_r($_POST); exit;
		if(isset($_POST))
		{
			if(!isset($_POST['status'])){ $status=0; }
			else{ $status = $_POST['status']; }

			if(!$_POST['id_course'])
			{
				$this->mwelcome->addCourse(array(
					'school_id' => $this->session->userdata('school_id'),
					'board_id' => $_POST['board_id'],
					'course_name' => $_POST['name'],
					'code' => $_POST['code'],
					'status' => $status
				));
			}
			else
			{
				$this->mwelcome->updateCourse(array(
					'id_course' => decode($_POST['id_course']),
					'board_id' => $_POST['board_id'],
					'course_name' => $_POST['name'],
					'code' => $_POST['code'],
					'status' => $status
				));
			}

			redirect(BASE_URL.'index.php/admin/course');
		}
	}

	function deleteCourse($id)
	{
		$this->mwelcome->deleteCourse(decode($id));
		echo json_encode(array('response' => 1,'data' =>''));
	}

	function subject()
	{
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/subject';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function getSubjectDataTable()
	{
		$results = json_decode($this->mwelcome->getSubjectDataTable($_POST));

		for($s=0;$s<count($results->data);$s++)
		{
			$results->data[$s][4] = encode($results->data[$s][4]);
		}
		echo json_encode($results);
	}

	function addSubject($subject_id=0)
	{
		if($subject_id===0){}
		else{
			$data['subject'] = $this->mwelcome->getSubject(array('id_subject' => decode($subject_id)));
		}
		//echo "<pre>";print_r($data['subject']); exit;
		$data['course'] = $this->mwelcome->getCourse(array('school_id' => $this->session->userdata('school_id'),'status' => 1));
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/add_subject';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function createSubject()
	{
		if(isset($_POST))
		{
			if(!isset($_POST['status'])){ $status=0; }
			else{ $status = $_POST['status']; }

			if(!$_POST['id_subject'])
			{
				$this->mwelcome->addSubject(array(
					'course_id' => $_POST['course_id'],
					'subject_name' => $_POST['name'],
					'subject_code' => $_POST['code'],
					'status' => $status
				));
			}
			else
			{
				$this->mwelcome->updateSubject(array(
					'id_subject' => decode($_POST['id_subject']),
					'course_id' => $_POST['course_id'],
					'subject_name' => $_POST['name'],
					'subject_code' => $_POST['code'],
					'status' => $status
				));
			}

			redirect(BASE_URL.'index.php/admin/subject');
		}
	}

	function deleteSubject($id)
	{
		$this->mwelcome->deleteSubject(decode($id));
		echo json_encode(array('response' => 1,'data' =>''));
	}

	function weekdays()
	{
		//echo date('N', strtotime('Monday')); exit;
		$data['week_day'] = $this->mwelcome->getWeekDay(array('school_id' => $this->session->userdata('school_id')));
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/weekdays';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function updateWeekDay()
	{
		if(isset($_POST)){
			$_POST['school_id'] = $this->session->userdata('school_id');
			$day = $this->mwelcome->getWeekDay($_POST);
			if(empty($day)){
				$this->mwelcome->addWeekDay($_POST);
			}
			else{
				$_POST['id_week_day'] = $day[0]['id_week_day'];
				$this->mwelcome->updateWeekDay($_POST);
			}

			echo json_encode(array('response' => 1, 'data' => ''));
		}
	}

	function classTiming()
	{
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/class_timing';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function getClassTimingDataTable()
	{
		$results = json_decode($this->mwelcome->getClassTimingDataTable($_POST));

		for($s=0;$s<count($results->data);$s++)
		{
			$results->data[$s][2] = date('g:i a', strtotime($results->data[$s][2]));
			$results->data[$s][3] = date('g:i a', strtotime($results->data[$s][3]));
			$results->data[$s][5] = encode($results->data[$s][5]);
		}
		echo json_encode($results);
	}

	function addClassTiming($class_timing=0)
	{
		if($class_timing===0){}
		else{
			$data['class_timing'] = $this->mwelcome->getClassTiming(array('id_class_timing' => decode($class_timing),'school_id' => $this->session->userdata('school_id')));
		}

		$data['course'] = $this->mwelcome->getCourse(array('school_id' => $this->session->userdata('school_id'),'status' => 1));
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/add_class_timing';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function createClassTiming()
	{
		if(isset($_POST))
		{
			if(!isset($_POST['is_break'])){ $is_break=0; }
			else{ $is_break = 1; }

			if(!$_POST['id_class_timing'])
			{
				$this->mwelcome->addClassTiming(array(
					'course_id' => $_POST['course_id'],
					'name' => $_POST['name'],
					'start_time' => $_POST['start_time'],
					'end_time' => $_POST['end_time'],
					'is_break' => $is_break
				));
			}
			else
			{
				$this->mwelcome->updateClassTiming(array(
					'id_class_timing' => decode($_POST['id_class_timing']),
					'course_id' => $_POST['course_id'],
					'name' => $_POST['name'],
					'start_time' => $_POST['start_time'],
					'end_time' => $_POST['end_time'],
					'is_break' => $is_break
				));
			}

			redirect(BASE_URL.'index.php/admin/classTiming');
		}
	}

	function deleteClassTiming($id)
	{
		$this->mwelcome->deleteClassTiming(decode($id));
		echo json_encode(array('response' => 1,'data' =>''));
	}

	function timeTable()
	{
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/time_table';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function getClassTimeTableDataTable()
	{
		$results = json_decode($this->mwelcome->getClassTimeTableDataTable($_POST));

		for($s=0;$s<count($results->data);$s++)
		{
			$results->data[$s][4] = encode($results->data[$s][4]);
		}
		echo json_encode($results);
	}

	function addTimetable($class_time_table=0)
	{
		if($class_time_table===0){}
		else{
			$data['class_timing'] = $this->mwelcome->getClassTimeTable(array('id_class_time_table' => decode($class_time_table),'school_id' => $this->session->userdata('school_id')));

			$days = $this->mwelcome->getWeekDay(array('school_id' => $this->session->userdata('school_id'),'status' => 1));
			$timings = $this->mwelcome->getClassTiming(array('course_id' => $data['class_timing'][0]['course_id'],'school_id' => $this->session->userdata('school_id')));
			if(empty($timings)){
				$html = '<p>There are no timings for this class <a href="'.BASE_URL.'index.php/admin/addClassTiming">Create Class Timing</a></p>';
			}
			else
			{
				$html='<table class="table table-bordered"><tr><th>WeekDays</th>';
				for($s=0;$s<count($timings);$s++){
					$html.='<th>'.date('g:i a', strtotime($timings[$s]['start_time'])).' '.date('g:i a', strtotime($timings[$s]['end_time'])).'</th>';
				}
				$html.='</tr><tbody>';

				for($s=0;$s<count($days);$s++)
				{	$html.='<tr><td>'.week_days($days[$s]['day']).'</td>';
					for($r=0;$r<count($timings);$r++)
					{
						if($timings[$r]['is_break']==1)
						{
							$html.='<td><div>Break</div><input type="hidden" name="time_id_day_id[]" value="'.$timings[$r]['id_class_timing'].'@@@'.$days[$s]['day'].'"></td>';
						}
						else{
							$sub_tea = $this->mwelcome->getTimeTableData(array('class_time_table_id' => $data['class_timing'][0]['id_class_time_table'],'class_timing_id' => $timings[$r]['id_class_timing'],'day_id' => $days[$s]['day']));

							if($sub_tea[0]['id_staff']=='' && $sub_tea[0]['id_subject']=='')
								$html.='<td><div><a href="javascript:;" onclick="getAssign(this,\''.$timings[$r]['id_class_timing'].'_'.$days[$s]['day'].'\');" data-toggle="modal" data-target="#myModal">Assign</a></div><input type="hidden" name="time_id_day_id[]" value="'.$timings[$r]['id_class_timing'].'@@@'.$days[$s]['day'].'"></td>';
							else{
								$html.='<td><div><div><span onclick="reAssign(this,\''.$timings[$r]['id_class_timing'].'_'.$days[$s]['day'].'\')" style="cursor: pointer;">X</span><p>'.$sub_tea[0]["first_name"].' '.$sub_tea[0]["last_name"].'</p><p>'.$sub_tea[0]["subject_name"].'</p><input type="hidden" value="'.$sub_tea[0]['id_staff'].'_'.$sub_tea[0]['id_subject'].'" name="t_'.$timings[$r]['id_class_timing'].'_'.$days[$s]['day'].'"></div></div><input type="hidden" name="time_id_day_id[]" value="'.$timings[$r]['id_class_timing'].'@@@'.$days[$s]['day'].'"></td>';
							}
						}

					}
					$html.='</tr>';
				}
				$html.='</table>';
			}
			$data['html'] = $html;

		}
		$data['course'] = $this->mwelcome->getCourse(array('school_id' => $this->session->userdata('school_id'),'status' => 1));
		$data['header']="header";
		$data['left_menu']="left_menu";
		$data['middle_content']='admin/add_time_table';
		$data['footer']='footer';
		$data['menu'] = 'settings';
		$this->load->view('landing',$data);
	}

	function getTimeTable()
	{
		if(isset($_POST))
		{
			$days = $this->mwelcome->getWeekDay(array('school_id' => $this->session->userdata('school_id'),'status' => 1));
			$timings = $this->mwelcome->getClassTiming(array('course_id' => $_POST['class_id'],'school_id' => $this->session->userdata('school_id')));
			if(empty($timings)){
				echo json_encode(array('response' => 2, 'data' => '')); exit;
			}
			else
			{
				$html='<table class="table table-bordered"><tr><th>WeekDays</th>';
				for($s=0;$s<count($timings);$s++){
					$html.='<th>'.date('g:i a', strtotime($timings[$s]['start_time'])).' '.date('g:i a', strtotime($timings[$s]['end_time'])).'</th>';
				}
				$html.='</tr><tbody>';

				for($s=0;$s<count($days);$s++)
				{	$html.='<tr><td>'.week_days($days[$s]['day']).'</td>';
					for($r=0;$r<count($timings);$r++)
					{
						if($timings[$r]['is_break']==0)
							$html.='<td><div><a href="javascript:;" onclick="getAssign(this,\''.$timings[$r]['id_class_timing'].'_'.$days[$s]['day'].'\');" data-toggle="modal" data-target="#myModal">Assign</a></div><input type="hidden" name="time_id_day_id[]" value="'.$timings[$r]['id_class_timing'].'@@@'.$days[$s]['day'].'"></td>';
						else
							$html.='<td><div>Break</div><input type="hidden" name="time_id_day_id[]" value="'.$timings[$r]['id_class_timing'].'@@@'.$days[$s]['day'].'"></td>';
					}
					$html.='</tr>';
				}
				$html.='</table>';
				echo json_encode(array('response' => 1, 'data' => $html)); exit;
			}

		}
	}

	function getStaffSubject()
	{
		if(isset($_POST)){
			$staff = $this->mwelcome->getStaff(array('school_id' => $this->session->userdata('school_id')));
			$subject = $this->mwelcome->getSubject(array('course_id' => $_POST['course_id'],'status' => 1));
			echo json_encode(array('response' => 1, 'staff' => $staff, 'subject' => $subject));
		}
	}

	function createTimeTable()
	{
		if(!$_POST['id_class_time_table'])
		{
			$data = array();
			$class_time_table_id = $this->mwelcome->addClassTimeTable(array('course_id' => $_POST['course_id'],'start_date' => date('Y-m-d',strtotime($_POST['start_date'])),'end_date' => date('Y-m-d',strtotime($_POST['end_date']))));
			for($s=0;$s<count($_POST['time_id_day_id']);$s++)
			{
				$t_s = explode('@@@',$_POST['time_id_day_id'][$s]);
				if(isset($_POST['t_'.$t_s[0].'_'.$t_s[1]])){
					$s_t = explode('-',$_POST['t_'.$t_s[0].'_'.$t_s[1]]);
				}
				else{
					$s_t[0] = '';
					$s_t[1] = '';
				}
				$data[] = array(
					'class_time_table_id' => $class_time_table_id,
					'class_timing_id' => $t_s[0],
					'day_id' => $t_s[1],
					'subject_id' => $s_t[1],
					'teacher_id' => $s_t[0],
				);
			}
			$this->mwelcome->addTimeTable($data);
			redirect(BASE_URL.'index.php/Admin/timeTable');
		}
		else
		{
			$class_time_table_id = decode($_POST['id_class_time_table']);
			$this->mwelcome->updateClassTimeTable(array('id_class_time_table' => $class_time_table_id,'course_id' => $_POST['course_id'],'start_date' => date('Y-m-d',strtotime($_POST['start_date'])),'end_date' => date('Y-m-d',strtotime($_POST['end_date']))));
			for($s=0;$s<count($_POST['time_id_day_id']);$s++)
			{
				$t_s = explode('@@@',$_POST['time_id_day_id'][$s]);
				if(isset($_POST['t_'.$t_s[0].'_'.$t_s[1]])){
					$s_t = explode('-',$_POST['t_'.$t_s[0].'_'.$t_s[1]]);
				}
				else{
					$s_t[0] = '';
					$s_t[1] = '';
				}
				$exe_time_table = $this->mwelcome->getTimeTableData(array('class_time_table_id' => $class_time_table_id,'class_timing_id' => $t_s[0],'day_id' => $t_s[1]));
				$data[] = array(
					'id_time_table' => $exe_time_table[0]['id_time_table'],
					'subject_id' => $s_t[1],
					'teacher_id' => $s_t[0],
				);
			}
			$this->mwelcome->updateTimeTable($data);
			redirect(BASE_URL.'index.php/Admin/timeTable');
		}
	}

	function deleteClassTimeTable($id)
	{
		$this->mwelcome->deleteTimeTable(decode($id));
		$this->mwelcome->deleteClassTimeTable(decode($id));
		echo json_encode(array('response' => 1,'data' =>''));
	}

}
