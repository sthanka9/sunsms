<?php
/**
 * Created by PhpStorm.
 * User: ASHOK
 * Date: 28/6/16
 * Time: 10:52 PM
 */

class Staff extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Datatables');
        $this->load->model("mdepartment");
        $this->load->model("mcategory");
        $this->load->model("mwelcome");
        $this->load->model("mstaff");
        if($this->session->userdata('user_id') && $this->session->userdata('user_type_id')==2){}
        else{
            redirect(BASE_URL);
        }
    }

    function index()
    {
        $data['header']="header";
        $data['left_menu']="left_menu";
        $data['middle_content']='staff/staff';
        $data['footer']='footer';
        $data['menu'] = 'staff';
        $this->load->view('landing',$data);
    }

    function staffDateTable()
    {
        $results = json_decode($this->mstaff->getStaffDataTable($_POST));
        for($s=0;$s<count($results->data);$s++)
        {
            $results->data[$s][5] = encode($results->data[$s][5]);
        }
        echo json_encode($results);
    }

    function createStaffView($id_staff=0)
    {
        $data['department'] = $this->mdepartment->getAllDepartments();
        $data['category'] = $this->mcategory->getAllCatgories();
        $data['county'] = $this->mwelcome->getCountry();
        if($id_staff===0){}
        else{
            $data['teacher_details'] = $this->mstaff->getStaffDetails(array('id_staff' => decode($id_staff)));
            $data['teacher_documents'] = $this->mstaff->getStaffDocuments(array('id_staff' => decode($id_staff)));
        }
        $data['blood_group'] = $this->mstaff->getAllBloodGroups();
        $data['header']="header";
        $data['left_menu']="left_menu";
        $data['middle_content']='staff/add_staff';
        $data['footer']='footer';
        $data['menu'] = 'staff';
        $this->load->view('landing',$data);
    }

    function createStaffDetails()
    {
        if(isset($_POST))
        {
            //print_r($_POST);
            //exit;
            if($_POST['id_staff'] == 0)
            {
                if(isset($_FILES['profile_pic']['name']) && $_FILES['profile_pic']['name']!=''){
                    $file_type = check_file_type($_FILES['profile_pic']['name'],'image');
                    if($file_type) {
                        $profile_pic = do_upload($_FILES['profile_pic']['name'], $_FILES['profile_pic']['tmp_name'],'');
                        $_POST['profile_pic'] = $profile_pic;
                    }
                }else{
                    $_POST['profile_pic'] = '';
                }

                $password = generate_password(6);
                $user_id = $this->mwelcome->addUser(array(
                    'user_type_id' => 2,
                    'first_name' => $_POST['first_name'],
                    'last_name' => $_POST['last_name'],
                    'email' => $_POST['email'],
                    'phone_number' => $_POST['phone_number'],
                    'password' => md5($password),
                ));

                $this->mstaff->addStaff(array(
                    'school_id' => 0,//$_POST['school_id'],
                    'user_id' => $user_id,
                    'teacher_number' => $_POST['teacher_number'],
                    'joining_date' =>  date('Y-m-d',strtotime($_POST['joining_date'])),
                    'first_name' => $_POST['first_name'],
                    'last_name' => $_POST['last_name'],
                    'gender' => $_POST['gender'],
                    'dob' => date('Y-m-d',strtotime($_POST['date_of_birth'])),
                    'teacher_department' => $_POST['teacher_department'],
                    'teacher_category' => $_POST['teacher_category'],
                    'qualification' => $_POST['qualification'],
                    'job_title' => $_POST['job_title'],
                    'year_id' => $_POST['year_id'],
                    'months_id' => $_POST['months_id'],
                    'experience_details' => $_POST['experience_details'],
                    'marital_status' => $_POST['marital_status'],
                    'blood_group_id' => $_POST['blood_group'],
                    'father_name' => $_POST['father_name'],
                    'mother_name' => $_POST['mother_name'],
                    'children_count' => $_POST['children_count'],
                    'nationality_id' => $_POST['nationality_id'],
                    'staff_status' => $_POST['staff_status'],
                    'email' => $_POST['email'],
                    'phone_number' => $_POST['phone_number'],
                    'profile_pic' => $_POST['profile_pic']
                ));
            }
            else
            {

                $this->mstaff->updateStaff(array(
                    'id_staff' => $_POST['id_staff'],
                    'teacher_number' => $_POST['teacher_number'],
                    'joining_date' =>  date('Y-m-d',strtotime($_POST['joining_date'])),
                    'first_name' => $_POST['first_name'],
                    'last_name' => $_POST['last_name'],
                    'gender' => $_POST['gender'],
                    'dob' => date('Y-m-d',strtotime($_POST['date_of_birth'])),
                    'teacher_department' => $_POST['teacher_department'],
                    'teacher_category' => $_POST['teacher_category'],
                    'qualification' => $_POST['qualification'],
                    'job_title' => $_POST['job_title'],
                    'year_id' => $_POST['year_id'],
                    'months_id' => $_POST['months_id'],
                    'experience_details' => $_POST['experience_details'],
                    'marital_status' => $_POST['marital_status'],
                    'blood_group_id' => $_POST['blood_group'],
                    'father_name' => $_POST['father_name'],
                    'mother_name' => $_POST['mother_name'],
                    'children_count' => $_POST['children_count'],
                    'nationality_id' => $_POST['nationality_id'],
                    'email' => $_POST['email'],
                    'phone_number' => $_POST['phone_number'],
                    'staff_status' => $_POST['staff_status']
                ));
            }
            redirect(BASE_URL.'index.php/staff');
        }
    }

    function createStaffContactDetails()
    {
        if(isset($_POST))
        {
             $this->mstaff->updateStaff(array(
                    'id_staff' => $_POST['id_staff'],
                    'home_address_line1' => $_POST['home_address_line1'],
                    'home_address_line2' => $_POST['home_address_line2'],
                    'home_city' => $_POST['home_city'],
                    'home_state' => $_POST['home_state'],
                    'home_country_id' => $_POST['home_country_id'],
                    'home_pincode' => $_POST['home_pincode'],

                ));
            redirect(BASE_URL.'index.php/staff');
        }
    }

    function deleteStaff($id)
    {
        $this->mstaff->deleteStaff(decode($id));
        echo json_encode(array('response' => 1,'data' =>''));
    }

    function createStaffDocuments(){
        if(isset($_POST))
        {
            if($_POST['id_staff'] !=0)
            {
                if($_POST['deleted_documents_id']!=''){
                    $ids=explode(',',$_POST['deleted_documents_id']);
                    $this->mstaff->deleteStaffDocuments($ids);
                }
                if(isset($_FILES['documents']['name']) && $_FILES['documents']['name']!=''){
                    for($i=0;$i<count($_FILES['documents']['size']);$i++){
                        if(isset($_FILES['documents']['name'][$i]) && !empty($_FILES['documents']['name'][$i])){
                            $actual_name=$_FILES['documents']['name'][$i];
                            $folder_name='staff';
                            $file_source_name = do_upload($_FILES['documents']['name'][$i], $_FILES['documents']['tmp_name'][$i],$folder_name);
                            $this->mstaff->addStaffDocuments(array(
                                'staff_id' => $_POST['id_staff'],
                                'document_name' => $actual_name,
                                'document_type' => '',
                                'document_source' => $file_source_name
                            ));
                        }
                    }
                }

            }
            redirect(BASE_URL.'index.php/staff');
     }
    }


}