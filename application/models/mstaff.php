<?php
/**
 * Created by PhpStorm.
 * User: ASHOK
 * Date: 1/7/16
 * Time: 7:51 PM
 */

class MStaff extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('mcommon');
    }

    function getStaffDataTable($data)
    {
        $this->datatables->select('s.teacher_number,s.first_name, s.gender,s.qualification, s.staff_status, s.id_staff',FALSE)
            ->from('staff As s');
        return $this->datatables->generate();
    }

    public function getStaffDetails($data){
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('id_staff',$data['id_staff']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getAllBloodGroups(){
        $this->db->select('*');
        $this->db->from('ss_blood_group');
        $query = $this->db->get();
        return $query->result_array();
    }

    function addStaff($data)
    {
        $this->db->insert('staff',$data);
        return $this->db->insert_id();
    }

    function updateStaff($data)
    {
        $this->db->where('id_staff',$data['id_staff']);
        $this->db->update('staff', $data);
    }

    public function deleteStaff($id_staff){
        $this->db->delete('staff', array('id_staff' => $id_staff));
    }

    public function getStaffDocuments($data){
        $this->db->select('*');
        $this->db->from('ss_staff_documents');
        $this->db->where('staff_id',$data['id_staff']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function addStaffDocuments($data){
        $this->db->insert('ss_staff_documents',$data);
        return $this->db->insert_id();
    }

    public function deleteStaffDocuments($data){
        if ($data) {
            for ($i = 0; $i <= count($data); $i++)
            {
                $this->db->delete('ss_staff_documents', array('id_staff_document' => $data[$i]));
            }
        }
    }
} 