<?php
class MStudent extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('mcommon');
    }
    function getStudentDataTable($data)
    {
        
        $this->datatables->select('CONCAT_WS(" ",s.first_name,s.middle_name,s.last_name) as sname,s.admission_number,s.gender,s.id_student',FALSE)
            ->from('ss_student As s')
            ->group_by('s.id_student');
        return $this->datatables->generate();
    }
    function addStudent($data){
        $this->db->insert('ss_student',$data);
        return $this->db->insert_id();
    }
    function updateStudent($data){
        
        $this->db->where('id_student',$data['id_student']);
        $this->db->update('ss_student', $data);
    }
    function getStudent($data){
        $this->db->select('s.*,c.id_city as city_id,c.state_id,st.country_id,s.fk_id_user as id_user,s.id_student');
        $this->db->from('ss_student s');
        $this->db->join('user u','s.fk_id_user=u.id_user');
        $this->db->join('city c','c.id_city=s.fk_city_id','left');
        $this->db->join('state st','st.id_state=c.state_id','left');
        if(isset($data['id_student']))
            $this->db->where('s.id_student', $data['id_student']);
        if(isset($data['user_id']))
            $this->db->where('s.user_id', $data['user_id']);
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result_array();
    }
}