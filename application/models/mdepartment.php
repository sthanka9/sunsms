<?php
/**
 * Created by PhpStorm.
 * User: ASHOK
 * Date: 28/6/16
 * Time: 10:12 PM
 */

class MDepartment extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('mcommon');
    }

    function getDepartmentDataTable($data)
    {
        $this->datatables->select('d.department_name,d.id_department',FALSE)
            ->from('departments As d');
        return $this->datatables->generate();
    }

     function getAllDepartments(){
         $this->db->select('*');
         $this->db->from('departments');
         $query = $this->db->get();
         return $query->result_array();
     }

    public function getDepartment($data){
        $this->db->select('*');
        $this->db->from('departments');
        $this->db->where('id_department',$data['department_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insertDepartment($post){
        $data = array(
            'department_name' => $post['department_name']
        );
        $this->db->insert('departments', $data);
    }
    public function updateDepartment($post){
        $data = array(
            'department_name' => $post['department_name']
        );

        $this->db->where('id_department', $post['id_department']);
        $this->db->update('departments', $data);
    }
    public function deleteDepartment($id_department){
        $this->db->delete('departments', array('id_department' => $id_department));
    }
} 